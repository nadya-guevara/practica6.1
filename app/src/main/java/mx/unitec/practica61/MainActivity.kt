package mx.unitec.practica61

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import android.widget.EditText
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    lateinit var txtDivisor: EditText
    lateinit var txtDividendo: EditText
    lateinit var fab: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txtDivisor = findViewById(R.id.txtDivisor)
        txtDividendo = findViewById(R.id.txtDividendo)
        fab = findViewById(R.id.fab)

        fab.setOnClickListener {

            var divisor: Int = txtDivisor.editableText.toString().toInt()
            var dividendo: Int = txtDividendo.editableText.toString().toInt()
            Snackbar.make(it, (divisor / dividendo).toString(), Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show()
        }
    }

    fun dividirNumeros(v: View){

        var divisor: Int = txtDivisor.editableText.toString().toInt()
        var dividendo: Int = txtDividendo.editableText.toString().toInt()

        Toast.makeText(this, (divisor / dividendo).toString(), Toast.LENGTH_SHORT).show()

    }
}
